import { Component, OnInit } from '@angular/core';
import { HomeService } from '../services/home.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  avis: string;
  errorMessage: String;
  successMessage: String;

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.errorMessage = "";
    this.successMessage = "";

  }



  OnSubmit(form: NgForm) {
    this.avis = form.value["avis"];
    this.homeService.save(this.avis).then(
      (a: any) => {
        console.log(a);
        form.reset();
        this.successMessage = "Votre avis a bien été envoyé !";
      },
      (error) => {
        console.log(error);
        this.errorMessage = error;
      }

    )

  }
}
