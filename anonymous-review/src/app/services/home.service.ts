import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as firebase from 'firebase';

@Injectable()
export class HomeService {
    constructor(){

    }

    save(avis:string){
        
        return new Promise(
            (resolve, reject) => {
                firebase.database().ref('/avis').push(avis).then(
                () => {     
                    resolve();
                },
                (error) => {
                reject(error);
                }
            );
        })
    }
}