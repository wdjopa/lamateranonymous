import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'anonymous-review';

  constructor () {
    var config = {
      apiKey: "AIzaSyDviGSrKZftzqG3xnmN9uon7kC7SJVH64M",
      authDomain: "lamateranonymousreviews.firebaseapp.com",
      databaseURL: "https://lamateranonymousreviews.firebaseio.com",
      projectId: "lamateranonymousreviews",
      storageBucket: "",
      messagingSenderId: "35012671087",
      appId: "1:35012671087:web:ea31900a79bbf17e91bf9a"
    };
    firebase.initializeApp(config);
  }
}
